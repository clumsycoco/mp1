//
//  convertionViewController.m
//  mp2
//
//  Created by SnowBiscuit on 5/11/14.
//  Copyright (c) 2014 SnowBiscuit. All rights reserved.
//

#import "convertionViewController.h"
#import "customCell.h"

@interface convertionViewController ()
{
   NSArray  *unitNames;
    NSMutableArray *unitValues;
    
    //double *enteredValue;
    NSMutableArray *valuesToBeDisplayed;
    NSMutableArray *textFields;
    int count ;
    
}

@end

@implementation convertionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    count = 0 ;
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(refreshClicked:)];
    
        self.navigationItem.rightBarButtonItem = refreshButton;
  
    
    unitNames = [self.currentDictionary allKeys];
   unitValues = [[NSMutableArray alloc] initWithArray:[self.currentDictionary allValues]];
    valuesToBeDisplayed = [[NSMutableArray alloc] initWithArray:unitValues];
    
    // Do any additional  loading the view.
}
- (IBAction)refreshClicked:(id)sender
{
    
    [self.myTableView reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
   customCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell" ];
    
    if(!cell)
    {
        cell = [[customCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mainCell"];
    }
    
    cell.label.text = [unitNames objectAtIndex:indexPath.row];
    
    NSString *b = [unitNames objectAtIndex:0];
    
    if([b isEqualToString:@"Fahreinheit"])
    {
        
        [  valuesToBeDisplayed replaceObjectAtIndex:0 withObject:@"373.15"];
        [  valuesToBeDisplayed replaceObjectAtIndex:1 withObject:@"212"];

        [  valuesToBeDisplayed replaceObjectAtIndex:2 withObject:@"100"];

        
        
    }
    
    NSNumber  *calculatedResult = [valuesToBeDisplayed objectAtIndex:indexPath.row];
    double result = [calculatedResult doubleValue];
    
    if(count==0)
     {
         result=1/result;
     }
    
    NSString *aString = [NSString stringWithFormat:@"%f", result];
    cell.textField.text = aString;
    
    
    
    
    
    
    return cell;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%@",[textFields objectAtIndex:indexPath.row]);
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return unitNames.count;
}



- (IBAction)edit:(UITextField *)sender
{
    count = 1;
        UIView *parentCell = [sender superview];
        
        while (![parentCell isKindOfClass:[UITableViewCell class]]) {
            parentCell = parentCell.superview;
        }
        UIView *parentView = parentCell.superview;
        
        while (![parentView isKindOfClass:[UITableView class]]) {
            parentView = parentView.superview;
        }
        UITableView *tableView = (UITableView *)parentView;
        NSIndexPath *indexPath = [tableView indexPathForCell:(UITableViewCell *)parentCell];
        NSInteger rowId = indexPath.row;
     //   NSLog(@"%ld",(long)indexPath.row);
        
        
        double enteredValue = [sender.text doubleValue];
     double coeff1 = [[unitValues objectAtIndex:rowId] doubleValue];
   // NSLog (@"%f",enteredValue);
    
    
    
        for(int i=0;i<unitValues.count;i++)
        {
            if (i!=rowId)
            {
               
                double coeff2 = [[unitValues objectAtIndex:i] doubleValue];
                double result = ( enteredValue*coeff1)/coeff2;
                
                NSString *temp = [NSString stringWithFormat:@"%f", result];
                NSLog(@"%@,%@",temp,[unitNames objectAtIndex:i]);
                [valuesToBeDisplayed replaceObjectAtIndex:i withObject:temp];
                
            }
            
        }
    
   NSString *temp1 = [NSString stringWithFormat:@"%f", enteredValue];
        
        [valuesToBeDisplayed replaceObjectAtIndex:rowId withObject:temp1];
    NSLog(@"%@,%@",temp1,[unitNames objectAtIndex:rowId]);
    
    
    
}
@end
