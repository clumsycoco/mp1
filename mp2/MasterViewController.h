//
//  MasterViewController.h
//  mp2
//
//  Created by SnowBiscuit on 4/19/14.
//  Copyright (c) 2014 SnowBiscuit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterViewController : UITableViewController
@property(strong,nonatomic) NSArray *headings;
@property(strong,nonatomic) NSDictionary *convertionsd;
@end
