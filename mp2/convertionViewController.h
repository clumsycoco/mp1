//
//  convertionViewController.h
//  mp2
//
//  Created by SnowBiscuit on 5/11/14.
//  Copyright (c) 2014 SnowBiscuit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface convertionViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *myTableView;
@property (strong,nonatomic) NSDictionary *currentDictionary;

- (IBAction)edit:(UITextField *)sender;




@end
