//
//  MasterViewController.m
//  mp2
//
//  Created by SnowBiscuit on 4/19/14.
//  Copyright (c) 2014 SnowBiscuit. All rights reserved.
//

#import "MasterViewController.h"

#import "convertionViewController.h"

@interface MasterViewController () {
    NSMutableArray *categories;
    NSDictionary *convertions;
    
  
}
@end

@implementation MasterViewController



- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"headingList" ofType:@"plist"];
    
   convertions =[[NSDictionary alloc] initWithContentsOfFile:path];
    
   categories  = [ convertions objectForKey:@"Categories"];
    

    

   /* self.navigationItem.leftBarButtonItem = self.editButtonItem;

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;*/
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*- (void)insertNewObject:(id)sender
{
    if (!categories) {
        _objects = [[NSMutableArray alloc] init];
    }
    [_objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}*/

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return categories.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headingCell" forIndexPath:indexPath];
    long row = [indexPath row];
    
    cell.textLabel.text = categories[row] ;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [categories removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    convertionViewController *transferViewController = segue.destinationViewController;
    
    if ([[segue identifier] isEqualToString:@"showDetail"])
    
    {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
            long row = [indexPath row];
        
        NSString *x = categories[row];

     transferViewController.currentDictionary  = [convertions objectForKey:x];
     
    }
}

@end
